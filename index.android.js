/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

import { Navigator } from 'react-native-deprecated-custom-components';

import CenaPrincipal from './src/components/CenaPrincipal';
import CenaClientes from './src/components/CenaClientes';
import CenaContatos from './src/components/CenaContatos';
import CenaEmpresa from './src/components/CenaEmpresa';
import CenaServico from './src/components/CenaServico';


export default class ATMServicos extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{ id: 'Principal' }}
        renderScene={(route, navigator) => {
          //definir a cena com base na rota
          switch (route.id) {
						case 'Principal':
							return (<CenaPrincipal navigator={navigator} />);

						case 'Cliente':
							return (<CenaClientes navigator={navigator} />);

						case 'Contato':
							return (<CenaContatos navigator={navigator} />);

						case 'Empresa': 
							return (<CenaEmpresa navigator={navigator} />);

						case 'Servico':
							return (<CenaServico navigator={navigator} />);

						default:
							return false;
					}
        }}
      />
    );
  }
}


AppRegistry.registerComponent('ATMServicos', () => ATMServicos);
