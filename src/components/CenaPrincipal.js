/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  StyleSheet,
  TouchableHighlight
} from 'react-native';

import BarraNavegacao from './BarraNavegacao'

const logo = require('../img/logo.png');
const menuCliente = require('../img/menu_cliente.png');
const menuContato = require('../img/menu_contato.png');
const menuEmpresa = require('../img/menu_empresa.png');
const menuServico = require('../img/menu_servico.png');

export default class CenaPrincipal extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#FFF'}}>
        <StatusBar 
          backgroundColor ='#CCC'
        />
        <BarraNavegacao />
        
        <View style={styles.logo}>
            <Image source={logo}/>
            
        </View>
        <View style={styles.menu}>
            <View style={styles.menuGrupo}>
                <TouchableHighlight
                    underlayColor={'#B9C941'}
                    activeOpacity={0.3} 
                    onPress={()=> {
                        this.props.navigator.push({ id : 'Cliente' });
                    }}> 
                    <Image style={styles.imgMenu} source={menuCliente}/>
                </TouchableHighlight>
                
                <TouchableHighlight
                     underlayColor={'#61BD8C'}
                     activeOpacity={0.3} 
                     onPress={()=> {
                        this.props.navigator.push({ id : 'Contato' });
                    }}
                >
                    <Image style={styles.imgMenu} source={menuContato}/>
                </TouchableHighlight>
            </View>
            <View style={styles.menuGrupo}>
                <TouchableHighlight
                    underlayColor={'#EC7148'}
                    activeOpacity={0.3} 
                    onPress={()=> {
                        this.props.navigator.push({ id : 'Empresa' });
                    }}
                >
                    <Image style={styles.imgMenu} source={menuEmpresa}/>
                </TouchableHighlight>

                <TouchableHighlight
                    underlayColor={'#19D1C8'}
                    activeOpacity={0.3} 
                    onPress={()=> {
                        this.props.navigator.push({ id : 'Servico' });
                    }}
                >
                    <Image style={styles.imgMenu} source={menuServico}/>
                </TouchableHighlight>
            </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
    logo:{
        marginTop:30,
        alignItems: 'center'
    },
    menu:{
        alignItems: 'center'
    },
    menuGrupo:{
        flexDirection: 'row'
    },
    imgMenu:{
        margin: 15
    }
})